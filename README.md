Fork from https://github.com/delordson/AssetManager.git by Delordson Kallon

- The Asset Manager App is a total equipment management system designed to help organisations with a large number of assets, many of which are supplied by different companies and which need regular preventative maintenance and servicing often by yet another set of companies. The Asset Manager App allows companies to track which assets they own, wh… 
- upgraded with VS2015

# Asset Manager

[![](Resources/AssetsMain.png "Main")](Resources/AssetsMain.png)

The Asset Manager App is a total equipment management system designed to help organisations with a large number of assets, many of which are supplied by different companies and which need regular preventative maintenance and servicing often by yet another set of companies. The Asset Manager App allows companies to track which assets they own, what the maintenance history is of those assets are, when the next service is due and details of which company provides the service cover, including details of the value of the service contract. The Asset Manager App also provides for email notification to staff who need to be informed whenever the status of an asset changes.  

## Modules

The Asset Manager App Modules includes a Home Page, the Manage Assets Home Page,  the Manage Service Contracts Module, the Manage Service Contract Suppliers Module and the Settings Module.  

## Access

Access is controlled through a log in screen. The administrator uses an associated Silverlight application to configure new users, create roles and permissions for those roles as well as to assign users to roles. [![](Resources/screenshot_10282013_170633.png "screenshot_10282013_170633")](Resources/screenshot_10282013_170633.png)  

## The Home Screen

Access to all the features of the Asset Manager App is from the home page. From here, users can navigate to the ‘Manage Assets Module’, the ‘Manage Service Contracts Module’, the ‘Manage Service Contract Suppliers Module’ and the ‘Settings Module’. [![](Resources/screenshot_10282013_130908.png "screenshot_10282013_130908")](Resources/screenshot_10282013_130908.png)    

## Manage Assets

Clicking or tabbing on the Manage Assets button takes the user to the ‘Browse Assets by Asset Type page’. On this page, the user of presenteed with a list of assets types (configured in the settings). [![](Resources/screenshot_10282013_131118.png "screenshot_10282013_131118")](Resources/screenshot_10282013_131118.png)     Clicking or tabbing on any of these asset types takes the user to a list of assets of that asset type. [![](Resources/screenshot_10282013_131203.png "screenshot_10282013_131203")](Resources/screenshot_10282013_131203.png)     Clicking or tabbing on any asset takes the user to a page for managing that asset. This page provides for full traceability of the asset/equipment across its entire life cycle from purchase to eventual disposal. You can track the purchase details, manufacturer, supplier, current location, service history, service intervals and service contract details. Users can also subscribe to any asset to get an automated email whenever the asset details are updated.   The page is laid out in a series of tabs. The first tab presents key information about the asset/equipment such as its reference number in the asset register, the serial number a description of the asset, a description as well as an option to upload an image of the asset. [![](Resources/screenshot_10282013_131210.png "screenshot_10282013_131210")](Resources/screenshot_10282013_131210.png)     Of course the built in LightSwitch validation of required fields and field lengths works great as expected… [![](Resources/screenshot_10282013_185153.png "screenshot_10282013_185153")](Resources/screenshot_10282013_185153.png)   …but we’ve also added multi field validation where it makes sense. So you can’t for instance commission an asset before it’s purchase date. [![](Resources/screenshot_10282013_185020.png "screenshot_10282013_185020")](Resources/screenshot_10282013_185020.png)     The activities tab is where the full service history of the asset is recorded. The maintenance activity types are configured in the settings by an administrator. [![](Resources/screenshot_10282013_131214.png "screenshot_10282013_131214")](Resources/screenshot_10282013_131214.png)     The Schedule tab gives a list of how often various maintenance activity types need to be performed for the asset. [![](Resources/screenshot_10282013_131218.png "screenshot_10282013_131218")](Resources/screenshot_10282013_131218.png)     The Contracts gives info on the service contracts that have been negotiated for the asset. [![](Resources/screenshot_10282013_131233.png "screenshot_10282013_131233")](Resources/screenshot_10282013_131233.png)   The Capital tab includes capital cost information. Some uses cases may require that you configure permissions so that only permitted users can see this info. That is not built in but should be easy enough to do. [![](Resources/screenshot_10282013_131238.png "screenshot_10282013_131238")](Resources/screenshot_10282013_131238.png)     The Subscribers tab allows interested parties to opt into email notification whenever any info on the asset changes. Changes tracked includes updates to the maintenance history. [![](Resources/screenshot_10282013_131243.png "screenshot_10282013_131243")](Resources/screenshot_10282013_131243.png)     Here’s an example of what the notification looks like:     Finally a Comments tab allows for an unlimited number of comments to be recorded against an asset. [![](Resources/screenshot_10282013_131248.png "screenshot_10282013_131248")](Resources/screenshot_10282013_131248.png)    

## Manage Service Contracts

Many organisations have to have service contracts in place for the ppm of their assets and equipment for compliance purposes. This is where the Manage Service Contracts module come in handy. To navigate to it, from the Home page, click or tab on the ‘Manage Service Contracts’ icon. This opens the ‘Browse Service Contracts’ page listing all service contracts. [![](Resources/screenshot_10292013_201046.png "screenshot_10292013_201046")](Resources/screenshot_10292013_201046.png)     Clicking or tabbing on any of these takes the user to the specific page for that service contract. The page is laid out in two tabs. A details tab and an assets covered tab. The details tab includes information such as the contract start and end dates as well as details of the contract supplier. [![](Resources/screenshot_10282013_175029.png "screenshot_10282013_175029")](Resources/screenshot_10282013_175029.png)     The Assets Covered tab lists assets currently covered by the contract… [![](Resources/screenshot_10282013_175033.png "screenshot_10282013_175033")](Resources/screenshot_10282013_175033.png)     …as well as allowing the user to add assets from the asset register to the contract. [![](Resources/screenshot_10282013_175040.png "screenshot_10282013_175040")](Resources/screenshot_10282013_175040.png)    

## Manage Service Contract Suppliers

The Asset Manager App includes a page for managing service contract suppliers. Clicking or tabbing on the Manage Service Contract Suppliers icon takes the user to the ‘Browse Contract Suppliers’ page. [![](Resources/screenshot_10292013_201053.png "screenshot_10292013_201053")](Resources/screenshot_10292013_201053.png)     Clicking or tabbing on any item takes the user to the details page for that contract supplier. This page is laid out in two tabs. A details tab and a Contracts tab. The details tab allows users to record contractor information such as address and key contact. [![](Resources/screenshot_10282013_180144.png "screenshot_10282013_180144")](Resources/screenshot_10282013_180144.png)     The Contracts tab allows users to manage contracts for that supplier. [![](Resources/screenshot_10282013_180527.png "screenshot_10282013_180527")](Resources/screenshot_10282013_180527.png)    

## Settings

The Settings Module is the key module used by the administrator and any other users given the right permissions for managing key application reference data. Items managed include Asset Types, Maintenance Types, lists of Asset manufacturers, lists of Asset Suppliers and lists of Employees. [![](Resources/screenshot_10282013_131008.png "screenshot_10282013_131008")](Resources/screenshot_10282013_131008.png)

**Asset Types**

[![](Resources/screenshot_10292013_200944.png "screenshot_10292013_200944")](Resources/screenshot_10292013_200944.png)

**Maintenance Types**

[![](Resources/screenshot_10292013_200950.png "screenshot_10292013_200950")](Resources/screenshot_10292013_200950.png)

**Asset Manufacturers**

[![](Resources/screenshot_10292013_200956.png "screenshot_10292013_200956")](Resources/screenshot_10292013_200956.png)

**Asset Suppliers**

[![](Resources/screenshot_10292013_201002.png "screenshot_10292013_201002")](Resources/screenshot_10292013_201002.png)

**Employees**

[![](Resources/screenshot_10292013_201007.png "screenshot_10292013_201007")](Resources/screenshot_10292013_201007.png)